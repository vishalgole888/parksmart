angular.module('Parksmart.services', ['Parksmart.config', 'ngCordova']).factory('Network', function($http, URL) {
	var self = this;

	self.post = function(data, url) {

		var request = $http({
			method : "POST",
			url : URL.base_value + url,
			headers : {
				"Content-Type" : "application/json",
				"Access-Control-Allow-Origin" : "*"
			},
			data : data
		});
		console.log(request);
		return request;
	};

	self.get = function(params, url) {

		var request = $http({
			method : "GET",
			url : URL.base_value + url,
			params : params
		});
		return request;
	};

	return self;

}).factory('onlineStatus', ["$window", "$rootScope",
function($window, $rootScope,$cordovaNetwork) {
	var onlineStatus = {};
	
	onlineStatus.onLine = navigator.onLine;//$cordovaNetwork.isOnline();//isOnline();//

	onlineStatus.isOnline = function() {
		return onlineStatus.onLine;
	};

	$window.addEventListener("online", function() {
		onlineStatus.onLine = true;
		$rootScope.$digest();
	}, true);

	$window.addEventListener("offline", function() {
		onlineStatus.onLine = false;
		$rootScope.$digest();
	}, true);

	return onlineStatus;
}]).factory('DB', function($q, DB_CONFIG, $cordovaSQLite) {
	var self = this;
	self.db = null;
	//var DBName = "ParkSmart";

	self.init = function() {

		self.db = window.openDatabase(DB_CONFIG.name, '1.0', 'database', 500000);

		//self.db = $cordovaSQLite.openDB(DB_CONFIG.name);
		angular.forEach(DB_CONFIG.tables, function(table) {
			var columns = [];

			angular.forEach(table.columns, function(column) {
				columns.push(column.name + ' ' + column.type);
			});

			var primaryArray = [];
			angular.forEach(table.primary, function(primary) {
				primaryArray.push(primary.name);
			});

			if (primaryArray.length <= 0) {

				var query = 'CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ')';
				self.query(query);

			} else {
				var query = 'CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ', PRIMARY KEY (' + primaryArray.join(',') + '))';
				console.log(query);
				self.query(query);
			}
			console.log('Table ' + table.name + ' initialized');
		});
	};
	self.query = function(query, bindings) {
		bindings = typeof bindings !== 'undefined' ? bindings : [];
		var deferred = $q.defer();
		self.db.transaction(function(transaction) {
			transaction.executeSql(query, bindings, function(transaction, result) {
				deferred.resolve(result);
			}, function(transaction, error) {
				console.log(error);
				deferred.reject(error);
			});
		});

		return deferred.promise;
	};

	self.fetchAll = function(result) {
		var output = [];

		for (var i = 0; i < result.rows.length; i++) {
			output.push(result.rows.item(i));
		}

		return output;
	};

	self.fetch = function(result) {

		return result.rows.item(0);
	};

	self.deleteAll = function() {
		angular.forEach(DB_CONFIG.tables, function(table) {
			var query = 'DELETE FROM ' + table.name;
			self.query(query);
		});
	};

	return self;
})
// Resource service example
.factory('VehiclesParked', function(DB) {
	var self = this;

	self.all = function() {
		return DB.query('SELECT * FROM vehiclesParked').then(function(result) {
			return DB.fetchAll(result);
		});
	};

	self.getById = function(id) {
		return DB.query('SELECT * FROM vehiclesParked WHERE id = ?', [id]).then(function(result) {
			return DB.fetch(result);
		});
	};

	self.getByLicence = function(licenceNumber, propertyId, parkedDate) {
		return DB.query('SELECT * from vehiclesParked WHERE licence_number=? AND parked_date =? AND property_id = ?', [licenceNumber, parkedDate, propertyId]).then(function(result) {
			return DB.fetchAll(result);
		});
	};

	self.getByPermit = function(permitNumber, propertyId, parkedDate) {

		return DB.query('SELECT * from vehiclesParked WHERE permit_number=? AND parked_date =? AND property_id = ?', [permitNumber, parkedDate, propertyId]).then(function(result) {
			console.log(result);
			return DB.fetchAll(result);
		});
	};

	self.insertProperties = function(property) {
		return DB.query('INSERT OR REPLACE INTO properties (property_id,property_name) VALUES (?,?)', [property.propertyId, property.propertyName]).then(function(result) {
			console.log(result);
			return DB.fetchAll(result);
		});
	};

	self.getProperties = function() {
		return DB.query('SELECT * FROM properties').then(function(result) {
			return DB.fetchAll(result);
		});
	};
	self.insert = function(vehicleInfo) {
		console.log(vehicleInfo);
		console.log(self.db);
		return DB.query('INSERT OR REPLACE INTO vehiclesParked (licence_number,permit_number,is_parked,parked_date,property_id,zone_id,tenant_id,tenant_name,tenant_permit_allowed,parking_allowed_zone,is_synced) VALUES (?,?,?,?,?,?,?,?,?,?,?)', [vehicleInfo.licenceNumber, vehicleInfo.permitNumber, vehicleInfo.isParked, vehicleInfo.parkedDate, vehicleInfo.propertyId, vehicleInfo.zoneId, vehicleInfo.tenantId, vehicleInfo.tenantName, vehicleInfo.tenantPermitAllowed, vehicleInfo.parkingAllowedZone, vehicleInfo.isSynced]).then(function(result) {
			console.log(result);
			return DB.fetchAll(result);
		});
	};

	self.insertTenantZone = function(tenantZone) {
		return DB.query('INSERT OR REPLACE INTO tenant_zone(tenant_id,zone_id,reserved)VALUES(?,?,?)', [tenantZone.tenantId, tenantZone.zoneId, tenantZone.reserved]).then(function(result) {
			return DB.fetchAll(result);
		});
	};
	self.getTenantZoneCount = function(tenantZone) {
		return DB.query('SELECT COUNT(*) as count FROM tenant_zone WHERE tenant_id=? AND zone_id=?', [tenantZone.tenantId, tenantZone.zoneId]).then(function(result) {
			console.log(result);
			return DB.fetch(result);
		});
	};
    
   
	self.updateTenantZone = function(tenantZone) {
		return DB.query('UPDATE tenant_zone SET reserved=? WHERE tenant_id=? AND zone_id=?', [tenantZone.reserved, tenantZone.tenantId, tenantZone.zoneId]).then(function(result) {
			return result.rowsAffected;
		});
	};
    
    
    
    /*  Updated 29/4  */
    
    self.insertTenantOccupiedReserved = function(tenantOccupiedReservedObj) {
      return DB.query('INSERT OR REPLACE INTO tenantOccupiedReserved(propertyId, zoneId, tenantName, tenantId, tenantZonePermit, tenantParkingOccupied, tenantParkingReserved)VALUES(?,?,?,?,?,?,?)', 
      [tenantOccupiedReservedObj.propertyId, tenantOccupiedReservedObj.zoneId, tenantOccupiedReservedObj.tenantName, tenantOccupiedReservedObj.tenantId, tenantOccupiedReservedObj.tenantZonePermit, tenantOccupiedReservedObj.tenantParkingOccupied, tenantOccupiedReservedObj.tenantParkingReserved  ]).then(function(result) {
			return DB.fetchAll(result);
		});
      
    };
    
    self.getTenantOccupiedReservedCount = function(tenantOccupiedReservedObj) {
		return DB.query('SELECT COUNT(*) as count FROM tenantOccupiedReserved WHERE propertyId=? AND zoneId=? AND tenantId=?', [tenantOccupiedReservedObj.propertyId, tenantOccupiedReservedObj.zoneId, tenantOccupiedReservedObj.tenantId]).then(function(result) {
			console.log(result);
			return DB.fetch(result);
		});
	};
    
    self.updateTenantOccupiedReserved = function(tenantOccupiedReservedObj) {
		return DB.query('UPDATE tenantOccupiedReserved SET tenantZonePermit=?,tenantParkingOccupied=?, tenantParkingReserved=? WHERE propertyId=? AND zoneId=? AND tenantId=?', [tenantOccupiedReservedObj.tenantZonePermit, tenantOccupiedReservedObj.tenantParkingOccupied, tenantOccupiedReservedObj.tenantParkingReserved, tenantOccupiedReservedObj.propertyId, tenantOccupiedReservedObj.zoneId, tenantOccupiedReservedObj.tenantId]).then(function(result) {
			return result.rowsAffected;
		});
	};
    
    self.getTenantOccupiedReserved = function(tenantOccupiedReservedObj) {
		return DB.query('SELECT * FROM tenantOccupiedReserved WHERE propertyId=? AND zoneId=? AND tenantId=?', [tenantOccupiedReservedObj.propertyId, tenantOccupiedReservedObj.zoneId, tenantOccupiedReservedObj.tenantId]).then(function(result) {
		   return DB.fetchAll(result);
		});
	};
    
    
     /*  End Updated 29/4  */
    
    
	self.insertParkingZone = function(parkingZone) {
		return DB.query('INSERT OR REPLACE INTO parkingZones (zone_id,zone_name,property_id) VALUES (?,?,?)', [parkingZone.zoneId, parkingZone.zoneName, parkingZone.propertyId]).then(function(result) {
			return result.rowsInserted;
		});
	};

	self.getParkingZone = function(propertyId) {
		return DB.query('SELECT * FROM parkingZones WHERE property_id = ?', [propertyId]).then(function(result) {
			return DB.fetchAll(result);
		});
	};


	self.getTenantId = function(parkingZoneId){
		return DB.query('SELECT tenant_id FROM tenant_zone WHERE zone_id=?',[parkingZoneId]).then(function(result){
			return DB.fetchAll(result);
		});
	};
	self.getParkingZoneName = function(parkingZoneId) {
		return DB.query('SELECT zone_name FROM parkingZones WHERE zone_id = ?', [parkingZoneId]).then(function(result) {
			return DB.fetchAll(result);
		});
	};

	self.delete = function(licenceNumber) {
		return DB.query('DELETE FROM vehiclesParked WHERE licence_number=?', [licenceNumber]).then(function(result) {

			return result.rowsAffected;
		});
	};

	self.update = function(licenceNumber, zoneId, isParked, parkedDate, isSynced) {
		console.log('Updating...');
		console.log(isParked);
		return DB.query('UPDATE vehiclesParked SET zone_id=?,is_parked=?,is_synced=? WHERE licence_number=? AND parked_date =?', [zoneId, isParked, isSynced, licenceNumber, parkedDate]).then(function(result) {
			console.log(result);
			return result.rowsAffected;
		});
	};

	self.updateSyncStatus = function(licenceNumber, parkedDate) {
		console.log('Updating...');
		return DB.query('UPDATE vehiclesParked SET is_synced=? WHERE licence_number=? AND parked_date =?', ['true', licenceNumber, parkedDate]).then(function(result) {
			console.log(result);
			return result.rowsAffected;
		});
	};

	self.getParkedInfo = function(permitNumber, isParked, parkedDate) {
		return DB.query('SELECT * FROM vehiclesParked WHERE permit_number=? AND is_parked=? AND parked_date=?', [permitNumber, isParked, parkedDate]).then(function(result) {
			console.log(result);
			return DB.fetchAll(result);
		});
	};

	self.updateParkedInfo = function(zoneId, licenceNumber, parkedDate, isParked) {
		return DB.query('UPDATE vehiclesParked SET zone_id=?,is_parked=?,is_synced=? WHERE licence_number=? AND parked_date =?', [zoneId, isParked, 'false', licenceNumber, parkedDate]).then(function(result) {

			return DB.fetchAll(result);
		});
	};

	self.getUnSyncedInfo = function() {

		return DB.query('SELECT * FROM vehiclesParked WHERE is_synced="false"').then(function(result) {
			console.log(result);
			return DB.fetchAll(result);
		});
	};

	self.getVehicleParkedCount = function(tenantId) {
		return DB.query('SELECT COUNT(*) as count FROM vehiclesParked WHERE tenant_id=? AND is_parked="true"', [tenantId]).then(function(result) {
			console.log(result);
			return DB.fetch(result);
		});
	};

	self.getVehicleParkedZoneCount = function(tenantId, zoneId) {

		return DB.query('SELECT COUNT(*) as count FROM vehiclesParked WHERE tenant_id=? AND zone_id=? AND is_parked="true"', [tenantId, zoneId]).then(function(result) {
			console.log(result);
			return DB.fetch(result);
		});
	};

	self.getReserved = function(tenantId, zoneId) {
		return DB.query('SELECT reserved FROM tenant_zone WHERE tenant_id=? AND zone_id=?', [tenantId, zoneId]).then(function(result) {
			console.log(result);
			return DB.fetch(result);
		});
	};

	self.getAllVehicles = function(propertyId,date,parkingZone){
		console.log(propertyId+","+date+","+parkingZone);
		try{
		return DB.query('SELECT * FROM vehiclesParked WHERE property_id=? AND parked_date=? AND parking_allowed_zone=?',[propertyId,date,parkingZone]).then(function(result){
			console.log(result);
			return DB.fetchAll(result);
		});
		}catch(error){
			coonsole.log(error);
		}
	};
	self.dropTables = function(tableName) {

		DB.deleteAll();
	};

	return self;
});

